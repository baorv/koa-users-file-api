# koa-users-file-api

## Run project

```bash
npm run dev
```

## Route:

* GET `/users`
* POST `/users/create`
* GET `/users/:id`
* PUT `/users/:id`
* DELETE `/users/:id`
