import * as userService from '../services/userService';

/**
 * Get all users from storage file and returns to client
 *
 * @param {object|*|Context} ctx
 * @returns {Promise<void>}
 */
export async function getUsers(ctx) {
  const allUsers = await userService.readUsers();
  ctx.body = allUsers;
}

/**
 * Create a new user from client request by append new user lines to user storage file
 *
 * @param {object|*|Context} ctx
 * @returns {Promise<void>}
 */
export async function createUser(ctx) {
  const newUserData = userService.getUserDataFromContext(ctx);

  const newUser = await userService.addUser(newUserData);

  ctx.body = newUser;
}

/**
 * Update an existing user with new information and save to user storage file
 *
 * @param {object|*|Context} ctx
 * @returns {Promise<void>}
 */
export async function updateUser(ctx) {
  const userId = ctx.params.id;

  const foundUser = await userService.hasUser(userId);

  if (foundUser === null) {
    ctx.statusCode = 404;
    ctx.body = 'Not Found';
    return;
  }

  const updateUserData = userService.getUserDataFromContext(ctx);

  const updatedUser = await userService.updateUser(updateUserData, userId);

  ctx.body = updatedUser;
}

/**
 * Get user with given id
 *
 * @param {object|*|Context} ctx
 * @returns {Promise<void>}
 */
export async function getUser(ctx) {
  const userId = ctx.params.id;

  // Look up a user from given id
  const foundUser = userService.hasUser(userId);

  if (foundUser === null) {
    ctx.statusCode = 404;
    ctx.body = 'Not Found';
    return;
  }
  ctx.body = foundUser;
}

/**
 * Remove an existing user from user storage file
 *
 * @param {object|*|Context} ctx
 * @returns {Promise<void>}
 */
export async function deleteUser(ctx) {
  const userId = ctx.params.id;

  // Look up a user from given id
  const foundUser = userService.hasUser(userId);

  if (foundUser === null) {
    ctx.statusCode = 404;
    ctx.body = 'Not Found';
    return;
  }

  await userService.deleteUser(userId);

  ctx.body = foundUser;
}

