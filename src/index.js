import Koa from 'koa';
import apiRouter from './routes/api';
import bodyParser from 'koa-bodyparser';

const app = new Koa();

app
  .use(async (ctx, next) => {
    try {
      await next();
    } catch (err) {
      ctx.status = err.status || 500;
      ctx.body = err.message;
      ctx.app.emit('error', err, ctx);
    }
  })
  .use(bodyParser())
  .use(apiRouter.routes())
  .use(apiRouter.allowedMethods({
    throw: true
  }));

app.on('error', (err, ctx) => {
  console.error(err);
  ctx.statusCode = 500;
  ctx.body = {
    error: err.message
  }
});
app.listen(3000, () => {
  console.log("HTTP is running on http://localhost:3000");
}); // Listen on port 3000
