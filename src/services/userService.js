import {existsSync, promises as fs, writeFileSync} from 'fs';
import path from 'path';
import pick from "lodash.pick";

const USER_STORAGE_FILE = path.resolve(__dirname, '../../storage/users.json');

const isUserStorageFileExist = existsSync(USER_STORAGE_FILE);
if (!isUserStorageFileExist) {
  writeFileSync(USER_STORAGE_FILE, '[]');
}


/**
 * Add new user to the user storage file
 *
 * @param {object} user
 * @returns {Promise<{object}>}
 */
export async function addUser(user) {

  // Get all users from storage file
  let users = await readUsers();

  // Append a new user
  const newUser = {
    ...user,
    id: generateID(users)
  };

  users = [
    ...users,
    newUser
  ];

  // Rewrite the storage file
  await writeUsers(users);

  return newUser;
}

/**
 * Update an existing user
 *
 * @param {object} data
 * @param {int|string} id
 * @returns {Promise<{object}>}
 */
export async function updateUser(data, id) {

  // Get all users from storage file
  const users = await readUsers();

  // Update user when user have id equals to param id
  const updatedUsers = users.map(user => user.id.toString() === id.toString() ? {...user, ...data} : user);

  // Rewrite file
  await writeUsers(updatedUsers);
  return {
    ...data,
    id
  };
}

/**
 * Delete a user with given id
 *
 * @param {int|string} id
 * @returns {Promise<void>}
 */
export async function deleteUser(id) {
  const users = await readUsers();
  const deletedUsers = users.filter(user => user.id.toString() !== id.toString());

  await writeUsers(deletedUsers);
}

/**
 * Look up a user from a list of users
 *
 * @param {Array} users
 * @param {int|string} userId
 * @returns {object|null}
 */
export function findUser(users, userId) {
  const matchUsers = users.filter(user => user.id.toString() === userId.toString());
  if (matchUsers.length === 0) {
    return null;
  }
  return matchUsers[0];
}

/**
 * Generate user id by increasing +1 from the last item
 *
 * @param users
 * @returns {number}
 */
export function generateID(users) {
  const lastUser = users[users.length - 1];
  if (!lastUser) {
    return 1;
  }
  return parseInt(lastUser.id) + 1;
}

/**
 * Read all users from user storage file
 *
 * @returns {Promise<Array>}
 */
export async function readUsers() {
  const usersData = await fs.readFile(USER_STORAGE_FILE, {encoding: 'utf-8'});
  return JSON.parse(usersData);
}

/**
 * Write users to the user storage file
 *
 * @param {Array} users
 * @returns {Promise<void>}
 */
export async function writeUsers(users) {
  await fs.writeFile(USER_STORAGE_FILE, JSON.stringify(users, null, 2));
}

/**
 * Get user data from client request
 *
 * @param ctx
 * @returns {Promise<*>}
 */
export function getUserDataFromContext(ctx) {
  return pick(ctx.request.body, [
    'name',
    'address',
    'username',
    'email',
    'address',
    'phone',
    'website'
  ]);
}

/**
 * Check has user
 *
 * @param {int|string} id
 * @returns {Promise<Object|null>}
 */
export async function hasUser(id) {
  const users = await readUsers();

  return findUser(users, id);
}
