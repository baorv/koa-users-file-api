import Router from 'koa-router';
import * as userController from '../controllers/userController';
import validate from 'koa-yup-validator';
import userSchema from "../schemas/userSchema";

const apiRouter = new Router();

apiRouter.get('/users', userController.getUsers);
apiRouter.post('/users/create', validate(userSchema), userController.createUser);
apiRouter.put('/users/:id', userController.updateUser);
apiRouter.get('/users/:id', userController.getUser);
apiRouter.delete('/users/:id', userController.deleteUser);

export default apiRouter;
