import * as yup from 'yup';

const userSchema = yup.object().shape({
  name: yup.string().required(),
  username: yup.string().required(),
  email: yup.string().email().required(),
  address: yup.string().required(),
  phone: yup.string().required(),
  website: yup.string().url().required(),
});

export default userSchema;
